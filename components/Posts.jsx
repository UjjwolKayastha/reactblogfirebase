import React from "react";
import { PageHeader } from "antd";

function Posts(props) {
  return (
    <div className="posts_container">
      <PageHeader
        style={{
          border: "1px solid rgb(235, 237, 240)"
        }}
        title="POSTS"
      />
    </div>
  );
}

export default Posts;
